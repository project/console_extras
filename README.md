# CONTENTS OF THIS FILE
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers
 
## INTRODUCTION
The Console Extras module extends the Drupal Console to save more development
time for developers.

## REQUIREMENTS
* Drupal Console

## INSTALLATION
* Option 1: Install as you would normally install a contributed Drupal module.
* Option 2: $ composer require 'drupal/console_extras:^1.0'.
   
## CONFIGURATION
Extra Drupal Console Commands:
- $ drupal generate:extra:batch
  - This command will generate a batch page that contains a batch form
    so you can use it to process your batch jobs.
- $ drupal generate:extra:form:multistep
  - This command will generate an Ajax multi-step form.

## MAINTAINERS
Current maintainers:
 * Jay Chen (Jay.Chen) - https://drupal.org/user/180060
