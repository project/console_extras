## Console Extras 8.x-1.1, 2018-10-07
- Fixed coding standards.
- Added validation to check if the batch route exists.

## Console Extras 8.x-1.0, 2018-10-06
- Adds extra Drupal Console Commands:
  - $ drupal generate:extra:batch
    - This command will generate a batch page that contains a batch form
      so you can use it to process your batch jobs.
  - $ drupal generate:extra:form:multistep
    - This command will generate an Ajax multi-step form.
